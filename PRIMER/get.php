﻿<?php 
  header('Content-Type: application/json'); 
  header("Cache-Control: no-cache, must-revalidate"); 
  
  $author = $_GET['author'];  
  switch($author)
  {
    case 'Пушкин':
	  $data = array('author' => 'Пушкин А.С.',
					'books' => array(array('book' => 'Руслан и Людмила', 'quantity' => 2500 ),
									 array('book' => 'Евгений Онегин', 'quantity' => 2700)));
	break;
    case 'Гоголь':
	  $data = array('author' => 'Гоголь Н.В.',
					'books' => array(array('book' => 'Ревизор', 'quantity' => 1500 ),
									 array('book' => 'Мертвые души', 'quantity' => 1700)));
	break;
  }
  echo json_encode($data);